var startApp = function () {
		var
			mobileMenuSelector = '#menu',
			mmApi,
			smallDisplay = false,
			$window = $(window),
			$document = $(document),
			$header = $('header'),
			$footer = $('footer'),
			resizeListener = function () {
				smallDisplay = $window.width() < 768; //$screen-xs

				// close mobile menu
				if (!$('button.navbar-toggle').is(':visible')) {
					mmApi.close();
				}
			},
			scrollListener = function () {

			},
			scrollBodyTo = function ($target) {
				var offset = $target.length ? $target.offset().top : 0;
				$("html:not(:animated),body:not(:animated)").animate({scrollTop: Math.max(0, offset)}, 500, 'swing');
			},
			documentReady = function (event) {

				// load fonts
				if (typeof WebFont != 'undefined' && typeof WebFontFamilies == 'array') {
					WebFont.load({
						google: {
							families: WebFontFamilies
						}
					});
				}

				// mobile menu
				$(mobileMenuSelector).mmenu({
					onClick: {
						close: function () {
							return $(this).attr('id') != 'mm-othersites';
						}
					}
				});
				mmApi = $(mobileMenuSelector).data("mmenu");
				$('button[data-target*="' + mobileMenuSelector + '"]').on('click', function () {
					mmApi.open();
				});

				// iframes
				$('.typography .media iframe')
					.addClass('embed-responsive-item')
					.removeAttr('width')
					.removeAttr('height')
					.wrap('<div class="embed-responsive embed-responsive-16by9"></div>');

				// fix tables & images
				$('.typography table').addClass('table').removeAttr('cellpadding');
				$('.typography img').addClass('img-responsive');

				// buttons
				$('button[data-loading-text]').click(function () {
					var btn = $(this);
					btn.button('loading');
					setTimeout(function () {
						btn.button('reset')
					}, 7000);
				});

				// trigger tabs etc.
				if (location.hash) {
					$('a[href$="' + location.hash + '"], a[data-target="' + location.hash + '"]').trigger('click');
				}

				// trigger resize
				$window.trigger('resize');
			}
		;

		// init listeners
		$document.ready(documentReady);
		$window
			.on('load', function () {
				$window.trigger('resize');
			})
			.on('orientationchange', function () {
				$window.trigger('resize');
			})
			.on('resize', function () {
				waitForFinalEvent(resizeListener, 'resizeListener', 10);
			})
			.on('scroll', scrollListener);
	},
	deferJS = function (method) {
		if (window.jQuery) {
			method();
		} else {
			setTimeout(function () {
				deferJS(method)
			}, 200);
		}
	};

deferJS(startApp);