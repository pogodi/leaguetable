<main class="container typography" role="main">
    $BreadCrumbs
	<div class="row">
		<div class="col-12">
			<h1 class="text-center">
                $Title
			</h1>

			<% loop $Beers %>
				<div class="beer<% if $IsJudged %> judged<% end_if %>">
					<h3>$Name, {$ABV}%</h3>
					<div class="entry_id">Entry #$ID</div>
					<div class="bjcpcat">$BjcpCategory.Title</div>
					<% if $JudgingSessionID %>
					<div class="session">Judging at: $JudgingSession.Title</div>
					<% end_if %>
					<br>
					<% if $IsJudgedAndConfirmed %>
						<a href="$BjcpRating.Scan.Link" target="_blank" class="btn btn-primary">Download Judging Sheet</a>
					<% end_if %>
					<% if $IsDeletable %>
						<a href="{$Top.Link}delete_beer?id=$ID" class="btn btn-primary">Delete</a>
					<% end_if %>
                    <% if $CheckInTime %><div class="checked_in">checked in $CheckInTime.Nice</div><% end_if %>
					<% if not $CheckInTime %><a href="{$Top.EditLink($ID)}" class="btn btn-primary">Edit</a><% end_if %>
				</div>
			<% end_loop %>

			<p class="top">
				<a href="{$RegisterBeerLink}" class="btn btn-primary">Register a Beer</a>
			</p>
		</div>
	</div>
</main>
