<main class="container typography" role="main">
    $BreadCrumbs
	<div class="row">
		<div class="col-12">
			<h2 class="text-center">
                $Title
			</h2>

            $RegisterBeerForm
		</div>
	</div>
</main>