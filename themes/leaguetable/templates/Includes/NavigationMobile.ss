<nav id="menu">
	<ul>
		<% cached 'NavigationMobile', $ID, $List('Page').max('LastEdited') %>
			<% loop $Menu(1) %>
				<li>
					<a href="$Link" data-target="#$URLSegment">
						$MenuTitle
					</a>
					<% if $AllChildren.Count %>
						<ul>
							<% loop $AllChildren %>
								<li>
									<a href="$Link">$MenuTitle</a>
									<% if $Children.Count %>
										<ul>
											<% loop $Children %>
												<li>
													<a href="$Link">$MenuTitle</a>
													<% if $Children.Count %>
														<ul>
															<% loop $Children %>
																<li>
																	<a href="$Link">$MenuTitle</a>
																</li>
															<% end_loop %>
														</ul>
													<% end_if %>
												</li>
											<% end_loop %>
										</ul>
									<% end_if %>
								</li>
							<% end_loop %>
						</ul>
					<% end_if %>
				</li>
			<% end_loop %>
		<% end_cached %>
	</ul>
</nav>