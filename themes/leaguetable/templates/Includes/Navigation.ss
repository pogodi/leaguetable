<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
</button>
<div class="navbar-collapse collapse" id="navbarContent">
	<ul class="navbar-nav mr-auto">
		<% loop $Menu(1) %>
			<li class="nav-item $LinkingMode 	<% if $LinkingMode != 'link' %>active<% else %>$LinkingMode<% end_if %><% if $Children.Count %> dropdown<% end_if %>">
			<a href="$Link" class="nav-link <% if $Children.Count %>dropdown-toggle" data-toggle="dropdown"<% else %>"<% end_if %> data-target="#$URLSegment">
				$MenuTitle
				<% if $Children.Count %><b class="caret"></b><% end_if %>
				</a>
				<% if $Children.Count %>
					<ul class="dropdown-menu" role="menu">
						<% loop $Children %>
							<li class="dropdown-item <% if $LinkingMode != 'link' %>active<% end_if %>">
								<a class="nav-link" href="$Link">$MenuTitle</a>
							</li>
						<% end_loop %>
					</ul>
				<% end_if %>
			</li>
		<% end_loop %>
	</ul>
</div>
