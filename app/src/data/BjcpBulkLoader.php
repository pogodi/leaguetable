<?php


use SilverStripe\Dev\CsvBulkLoader;

class BjcpBulkLoader extends CsvBulkLoader {

    public $columnMap = [
        'Number' => 'CategoryNumber',
        'BJCP Categories' => 'Category',
        'Styles' => 'Name',
        'Style Family' => 'Family',
        'Style History' => 'History',
        'Origin' => 'Origin',
        'Overall Impression' => 'Impression',
        'Comments' => 'Comments'
    ];

    public $duplicateChecks = [
        '#' => 'CategoryNumber'
    ];

}
