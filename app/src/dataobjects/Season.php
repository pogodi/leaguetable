<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 2:07 AM
 */

use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBDatetime;
use \SilverStripe\ORM\FieldType\DBText;

class Season extends DataObject {

	private static $db = [
		'Start' => DBDatetime::class,
		'End' => DBDatetime::class
	];

	private static $has_many = [
		'JudgingSessions' => JudgingSession::class
	];

	private static $summary_fields = ['ID', 'Title'];

	public function Title() {
		$return = DBText::create();
		$return->setValue($this->Start . ' - ' . $this->End);
		return $return;
	}
}