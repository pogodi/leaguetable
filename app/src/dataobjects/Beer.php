<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 2:09 AM
 */

use SilverStripe\AssetAdmin\Forms\PreviewImageField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldDataColumns;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\GridField\GridFieldExportButton;
use SilverStripe\Forms\GridField\GridFieldFilterHeader;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use \SilverStripe\ORM\DataObject;
use \SilverStripe\Security\Member;
use \SilverStripe\ORM\FieldType\DBDatetime;
use \SilverStripe\ORM\ArrayList;
use \SilverStripe\Security\Security;

class Beer extends DataObject {

	private static $db = array(
		'Name' => 'Varchar(255)',
		'ABV' => 'Double',
        'SpecialInfo' => 'Varchar(255)',
		'CheckInTime' => DBDatetime::class
	);

	private static $has_one = [
		'Brewer' => Member::class,
		'JudgingTable' => 'JudgingTable',
		'JudgingSession' => 'JudgingSession',
		'BjcpCategory' => 'BjcpCategory',
		'BjcpSecondCategory' => 'BjcpCategory',
		'BjcpRating' => 'BjcpRating'
	];

	private static $summary_fields = [
        'ID','Brewer.Name', 'Name', 'ABV', 'BjcpCategory.CategoryNumber', 'JudgingSession.SessionDate', 'BjcpRating.ID'
    ];

    public static function get_gridfieldconfig() {
        $gridFieldConfig = GridFieldConfig_RecordEditor::create();

        $exportButton = new GridFieldExportButton();
        $exportButton->setExportColumns([
            'ID' => 'ID',
            'Brewer.FirstName' => 'First Name',
            'Brewer.Surname' => 'Surname',
            'Brewer.Email' => 'Email',
            'Name' => 'Beer',
            'ABV' => 'ABV',
            'PrimaryBjcp' => function($record) {
                return $record;
            },
            'SecondaryBjcp' => function($record) {
                return $record;
            },
            'SpecialInfo' => 'Extra Info'
        ]);

        $gridFieldConfig->addComponents(
            $exportButton
        );


        return $gridFieldConfig;
    }

	public static function mine() {
		$currentUser = Security::getCurrentUser();
		if (!$currentUser) return ArrayList::create();
		return Beer::get()->filter('BrewerID', $currentUser->ID)->sort('Created', 'DESC');
	}

	public static function delete_mine($id) {
		$beer = Beer::mine()->filter('ID', $id)->first();
		if ($beer) $beer->delete();
	}

	public function getCMSFields() {
        $fields = parent::getCMSFields();

        if ($this->BjcpRatingID) {
            $fields->push(UploadField::create('JudgingSheer', 'Judging Sheet', ArrayList::create([$this->BjcpRating()->Scan()])));
        }

        return $fields;
    }

    public function IsJudged() {
		$hasRating = $this->BjcpRatingID ? true : false;
		if ($hasRating && $this->BjcpRating()->ScanID) {
            return true;
        }
		return false;
	}

    public function IsJudgedAndConfirmed() {
		$hasRating = $this->BjcpRatingID ? true : false;
		if ($hasRating && $this->BjcpRating()->Confirmed && $this->BjcpRating()->ScanID) {
            return true;
        }
		return false;
	}

	public function IsDeletable () {
		if ($this->IsJudged()) return false;
		if ($this->CheckInTime) return false;
		return true;
	}

	public function Category() {
	    $secondary = $secondCat = $this->BjcpSecondCategoryID ? '/' . $this->BjcpSecondCategory()->CategoryNumber : '';
        return $this->BjcpCategory()->CategoryNumber . $secondary;
    }

    public function PrimaryBjcp() {
        return $this->BjcpCategoryID ? $this->BjcpCategory()->Title() : '';
    }

    public function SecondaryBjcp() {
        return $this->BjcpSecondCategoryID ? $this->BjcpSecondCategory()->Title() : '';
    }
}
