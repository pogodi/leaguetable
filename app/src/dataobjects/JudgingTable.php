<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 2:14 AM
 */

use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Member;

class JudgingTable extends DataObject {

	private static $db = [
		'Number' => 'Int'
	];

	private static $has_one = [
		'JudgingSession' => 'JudgingSession'
	];

	private static $has_many = [
		'Judges' => Member::class,
		'Beers' => 'Beer'
	];
}