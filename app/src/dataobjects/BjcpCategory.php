<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 2:09 AM
 */

use SilverStripe\ORM\DataObject;

class BjcpCategory extends DataObject {

	private static $db = [
		'Name' => 'Varchar(255)',
		'CategoryNumber' => 'Varchar(255)',
		'Category' => 'Varchar(255)',
        'Family' => 'Varchar(255)',
        'History' => 'Text',
        'Origin' => 'Varchar(255)',
        'Impression' => 'Text',
        'Comments' => 'Text'
	];

	private static $has_many = [
		'Beers' => 'Beer'
	];

	private static $default_sort = ['CategoryNumber'];

	public function Title() {
		return $this->CategoryNumber . ' - ' .$this->Name;
	}
}
