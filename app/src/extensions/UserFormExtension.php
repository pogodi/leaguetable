<?php

use SilverStripe\ORM\DataExtension;

class UserFormExtension extends DataExtension {
	public function updateForm() {
		$this->owner->setAttribute('novalidate', 'novalidate');
	}
}