<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 2:22 AM
 */

use SilverStripe\ORM\DataExtension;

class BrewerExtension extends DataExtension {

	private static $has_many = [
		'Beers' => 'Beer'
	];
}