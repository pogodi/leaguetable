<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\NumericField;

class SiteConfigExtension extends DataExtension {

	private static $db = array(
		'FooterText' => 'HTMLText',
		'FacebookURL' => 'Varchar(255)',
		'TwitterURL' => 'Varchar(255)',
		'YouTubeURL' => 'Varchar(255)',
		'LinkedInURL' => 'Varchar(255)',
		'VimeoURL' => 'Varchar(255)',
		'GooglePlusURL' => 'Varchar(255)',
		'InstagramURL' => 'Varchar(255)',
		'SoundCloudURL' => 'Varchar(255)',
		'MyspaceURL' => 'Varchar(255)',
		'PinterestURL' => 'Varchar(255)',
		'TumblrURL' => 'Varchar(255)',
		'SessionPrice' => 'Double'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Footer', new HtmlEditorField('FooterText'));
		$fields->addFieldToTab('Root.Social', new TextField('FacebookURL', 'Enter the full URL of your Facebook page'));
		$fields->addFieldToTab('Root.Social', new TextField('TwitterURL', 'Enter the full URL of your Twitter page'));
		$fields->addFieldToTab('Root.Social', new TextField('YouTubeURL', 'Enter the full URL of your Youtube page'));
		$fields->addFieldToTab('Root.Social', new TextField('VimeoURL', 'Enter the full URL of your Vimeo page'));
		$fields->addFieldToTab('Root.Social', new TextField('LinkedInURL', 'Enter the full URL of your LinkedIn page'));
		$fields->addFieldToTab('Root.Social', new TextField('GooglePlusURL', 'Enter the full URL of your Google+ page'));
		$fields->addFieldToTab('Root.Social', new TextField('InstagramURL', 'Enter the full URL of your Instagram page'));
		$fields->addFieldToTab('Root.Social', new TextField('SoundCloudURL', 'Enter the full URL of your Soundcloud page'));
		$fields->addFieldToTab('Root.Social', new TextField('MyspaceURL', 'Enter the full URL of your MySpace page'));
		$fields->addFieldToTab('Root.Social', new TextField('PinterestURL', 'Enter the full URL of your Pinterest page'));
		$fields->addFieldToTab('Root.Social', new TextField('TumblrURL', 'Enter the full URL of your Tumblr page'));

		$fields->addFieldToTab('Root.LeagueTable', new NumericField('SessionPrice', 'Session Price in NZ$'));
	}

}