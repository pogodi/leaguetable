<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 2:17 AM
 */

use SilverStripe\ORM\DataExtension;

class JudgeExtension extends DataExtension {

	private static $db = [
		'CanTableCaptain' => 'Boolean'
	];

	private static $has_one = [
		'JudgingTable' => 'JudgingTable'
	];

	private static $belongs_many_many = [
		'JudgingSessions' => 'JudgingSession'
	];
}