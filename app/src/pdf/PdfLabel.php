<?php


use mikehaertl\wkhtmlto\Pdf;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\View\ArrayData;

class PdfLabel {

    public function printBeerLabel(ArrayData $labelObj) {
        $labelObj->AbsoluteUrl = $this->AbsoluteUrl();
        $printTemplate = $labelObj->renderWith('BeerLabel');
        $pdf = $this->setupLabels();
        $pdf->addPage($printTemplate);
        return $this->assembleLabels($pdf, 'beer_');
    }


    public function AbsoluteUrl() {
        $url = Director::absoluteBaseURL();
        return $url;
    }

    protected function setupLabels($css='item-label.css') {
        $pdf = new Pdf(array(
            'binary' => BASE_PATH . '/vendor/bin/wkhtmltopdf-amd64',
            'no-outline',         // Make Chrome not complain
            'margin-top'    => 0,
            'margin-right'  => 0,
            'margin-bottom' => 0,
            'margin-left'   => 0,
            'page-height' => 55,
            'page-width' => 101,

            // Default page options
            'disable-smart-shrinking',
            'user-style-sheet' => BASE_PATH . '/themes/leaguetable/css/' . $css,
        ));

        return $pdf;
    }

    protected function assembleLabels($pdf, $fileLabel = 'items') {
        $unique = uniqid();
        $fileName = sprintf($fileLabel . '_%s.pdf', $unique);
        $filePath = ASSETS_PATH . '/' . $fileName;
        $pdf->saveAs($filePath);

        $rotatedOutputName = sprintf($fileLabel . '_rotated_%s.pdf', $unique);
        $rotatedOutputPath = ASSETS_PATH . '/' . $rotatedOutputName;

        $command = 'pdftk %s cat 1-endeast output %s';

        $this->execute(sprintf($command, $filePath, $rotatedOutputPath));

        return $rotatedOutputName;
    }

    protected function execute ($Command, $Print = false) {
        if($Print) {
            echo "\n > ".$Command;
            @flush();
            @ob_flush();
        }

        $Echo = '';
        $OutPut = array();
        exec($Command.' 2>&1', $OutPut);
        $Exit = false;
        foreach($OutPut as $Str){
            $Echo .= "\n".$Str;
            if(strpos($Str, 'error') !== false || strpos($Str, 'fatal') !== false || strpos($Str, 'is not recognized as an internal or external command')) {
                $Exit = true;
            }
        }
        if($Print || $Exit) {
            echo $Echo."\n";
            @flush();
            @ob_flush();
        }
        if($Exit) {
            die();
        }

        return $OutPut;
    }

}
