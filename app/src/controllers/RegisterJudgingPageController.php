<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 4:08 AM
 */

use \SilverStripe\Security\Security;
use \SilverStripe\Control\HTTPRequest;

class RegisterJudgingPageController extends LeagueMemberPageController {

	private static $allowed_actions = array(
		'register'
	);

	public function register (HTTPRequest $request) {
		JudgingSession::upcoming()->Judges()->add(Security::getCurrentUser());

		$this->redirectBack();
	}

	public function IsRegistered() {
		return JudgingSession::upcoming()->Judges()->filter('ID', Security::getCurrentUser()->ID)->Count() ? true : false;
	}
}