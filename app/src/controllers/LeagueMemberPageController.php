<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 4:08 AM
 */

class LeagueMemberPageController extends PageController {

	public function RegisterBeerLink() {
		return RegisterBeerPage::get()->first()->Link();
	}

	public function Beers() {
		return Beer::mine();
	}

	public function UpcomingJudgingSession() {
		return JudgingSession::upcoming();
	}
}