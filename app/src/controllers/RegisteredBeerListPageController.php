<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 6:14 AM
 */

use SilverStripe\Control\Controller;
use \SilverStripe\Control\HTTPRequest;

class RegisteredBeerListPageController extends LeagueMemberPageController {

	private static $allowed_actions = array(
		'delete_beer', 'download', 'edit_beer'
	);

	public function delete_beer (HTTPRequest $request) {
		$id = intval($request->getVar('id'));
		if ($id) {
			Beer::delete_mine($id);
		}

		$this->redirectBack();
	}

    public function EditLink($id) {
        $editPage = RegisterBeerPage::get()->first();
        $result =  Controller::join_links($editPage->Link(), '?bid='. $id);

        return $result;

    }

	public function download(HTTPRequest $request) {
		//todo:make download work
	}
}
