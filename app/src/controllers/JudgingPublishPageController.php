<?php


use SilverStripe\Assets\Image;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Versioned\Versioned;
use SilverStripe\View\ArrayData;

class JudgingPublishPageController extends PageController {

    private static $allowed_actions = [
        'rotate'
    ];

    public function init() {
        parent::init();
    }

    public function Judgings() {
        $results = Versioned::get_by_stage('BjcpRating', Versioned::DRAFT)
                            ->exclude('Confirmed', true)
                            /*->filter('ID',21)*/
                            ->limit(5);

        return $results;
    }

    public function RotateLink() {
        return \SilverStripe\Control\Controller::join_links($this->Link(), '/rotate');
    }

    public function rotate($request) {
        Versioned::set_reading_mode('Stage.Stage');
        $beer = Beer::get()->filter('ID', $request->getVar('id'))->first();
        $rating = $beer->BjcpRating();
        $image = $rating->Scan();
        if ($image) {
            $backend = $image->getImageBackend();
            if ($backend->getImageResource()) {
                $tmp_image = ASSETS_PATH . '/new_results/' . mt_rand(100000, 999999) . '.' . $image->getExtension();
                $backend->setImageResource($backend->getImageResource()->rotate($request->getVar('top')));
                $backend->writeTo($tmp_image);
                $image->setFromLocalFile($tmp_image, $image->FileName); // set new image
                $image->write();

                $rating->Confirmed = true;
                $rating->write();

                Versioned::set_reading_mode('Stage.Live');
                //send email to user with link
                $brewer = $beer->Brewer();
                $email = Email::create()
                    ->setHTMLTemplate('Email\\ResultsEmail')
                    ->setData([
                     'Member' => $brewer,
                     'Link'=> RegisterBeerPage::get()->first()->AbsoluteLink(),
                    ])
                    ->setFrom(Config::inst()->get(Email::class, 'admin_email'))
                    ->setTo($brewer->Email)
                    ->setSubject(sprintf('Your judging result for "%s" is ready for viewing.', $beer->Name));

                $email->send();

                return json_encode(['success' => true]);
            }
        }
    }
}
