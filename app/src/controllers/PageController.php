<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
	use SilverStripe\View\Requirements;
	use SilverStripe\Core\Manifest\ModuleResourceLoader;

    class PageController extends ContentController {

        private static $allowed_actions = ['RegisterBeerForm'];

		protected function init() {
			parent::init();

			$loader = SilverStripe\View\ThemeResourceLoader::inst();
			$themes = SilverStripe\View\SSViewer::get_themes();

			Requirements::block(THIRDPARTY_DIR.'/jquery/jquery.js');
			Requirements::block(THIRDPARTY_DIR.'/jquery/jquery.min.js');
			Requirements::set_force_js_to_bottom(true);
			Requirements::combine_files(
				'app.js',
				array(
					$loader->findThemedJavascript('jquery.mmenu.min.js', $themes),
					$loader->findThemedJavascript('webfont.js', $themes),
					$loader->findThemedJavascript('helpers.js', $themes),
                    $loader->findThemedJavascript('typeahead.bundle.js', $themes),
                    $loader->findThemedJavascript('selectize.min.js', $themes),
					$loader->findThemedJavascript('main.js', $themes)
				),
				array(
					'defer' => true,
					'type' => ''
				)
			);
		}

        public function RegisterBeerForm() {
            return RegisterBeerForm::create($this, 'RegisterBeerForm');
        }
    }
}

