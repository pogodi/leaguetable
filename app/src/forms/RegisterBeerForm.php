<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 4:16 AM
 */

use SilverStripe\Forms\CheckboxField;
use \SilverStripe\Forms\Form;
use \SilverStripe\Forms\FormAction;
use \SilverStripe\Forms\FieldList;
use \SilverStripe\Forms\RequiredFields;
use \SilverStripe\Forms\HiddenField;
use \SilverStripe\Security\Security;

class RegisterBeerForm extends Form {

	private static $allowed_actions = array(
		'forTemplate',
		'doRegister',
	);

	public function __construct($controller, $name) {
		//needed for custom sub-class of weblisting functionality
		$this->controller = $controller;
		$this->name = $name;
		$this->class = get_class($this);

        $beer = $this->getBeer();

		$fields = $this->getFrontendFields($beer);
		if ($beer->ID) {
            $actions = new FieldList(
                FormAction::create('doRegister', 'Save Beer')
            );

        } else {
            $actions = new FieldList(
                FormAction::create('doRegister', 'Register Beer')
            );
        }

		$validator = RequiredFields::create();

		// construct
		parent::__construct($controller, $name, $fields, $actions, $validator);

		if ($beer->ID) {
            $this->loadDataFrom($beer);
        }
		//$this->disableSecurityToken();
	}

	protected function getBeer() {
        $beer = null;
        $beerID = $this->controller->getRequest()->requestVar('bid');
        if ($beerID) {
            $beer = Beer::get()->filter('ID', $beerID)->first();
        }
        if (!$beer) {
            $beer = Beer::create();
        }
        return $beer;
    }

	protected function getFrontendFields (Beer $beer) {
		$fields = $beer->getFrontEndFields();

		$currentUser = Security::getCurrentUser();
		//die (print_r($currentUser, true));
		$fields->removeByName('JudgingTableID');
		$fields->removeByName('JudgingSessionID');
		$fields->removeByName('BjcpRatingID');
		$fields->removeByName('CheckInTime');

		$brewerField = HiddenField::create('BrewerID');
        if (!$this->isShop()) {
            $brewerField = $brewerField->setValue($currentUser->ID);
        }
        $fields->replaceField('BrewerID', $brewerField);

		$fields->insertAfter('BjcpSecondCategory', $fields->dataFieldByName('SpecialInfo'));

		if ($this->isShop()) {
            $fields->insertAfter('SpecialInfo', CheckboxField::create('CheckedIn')->setValue(true));
        }
		if ($beer->ID) {
		    $fields->push(HiddenField::create('bid','bid', $beer->ID));
        }

		return $fields;
	}

	public function doRegister($data, Form $form) {
        $beer = $this->getBeer();

		$form->saveInto($beer);
		$session = JudgingSession::upcoming();
		if ($session) {
			$beer->JudgingSessionID = $session->ID;
		}

		if (isset($data['CheckedIn']) && $data['CheckedIn']) {
		    $beer->CheckInTime = time();
        }

        $beer->write();

		if ($this->getController() instanceof RegisterBeerPageController) {
            $RedirectPage = RegisteredBeerListPage::get()->first();
        } elseif ($this->isShop()) {
            $RedirectPage = ShopPage::get()->first();
        }
        $this->getController()->redirect($RedirectPage->Link());
	}

	protected function isShop() {
	    return $this->getController() instanceof ShopPageController;
    }
}
