<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 4:07 AM
 */

use \SilverStripe\Security\Security;

class LeagueMemberPage extends Page {
 // todo: make only accesible for league members

	public function canView($member = null) {
		if (!$member) {
			$member = Security::getCurrentUser();
		}

		if (!$member) return false;

		return true;
	}

}