<?php


use SilverStripe\Security\Permission;

class JudgingPublishPage extends Page {
    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain');
    }

}
