<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 3:49 AM
 */

use \SilverStripe\Admin\ModelAdmin;

class BjcpAdmin extends ModelAdmin {

	private static $managed_models = array('BjcpCategory');
	private static $url_segment = 'bjcp_categories';
	private static $menu_title = 'BJCP Categories';
	private static $model_importers = array();

	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id = null, $fields = null);
		$modelClass = $this->modelClass;
		if (method_exists($modelClass, 'get_gridfieldconfig')) {
			$form
				->Fields()
				->fieldByName($this->sanitiseClassName($this->modelClass))
				->setConfig($modelClass::get_gridfieldconfig());
		}

		return $form;
	}
}