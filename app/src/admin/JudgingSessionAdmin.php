<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 5/07/18
 * Time: 3:49 AM
 */

use \SilverStripe\Admin\ModelAdmin;

class JudgingSessionAdmin extends ModelAdmin {

	private static $managed_models = array('JudgingSession', 'Season', 'Beer', 'BjcpRating');
	private static $url_segment = 'judging_session';
	private static $menu_title = 'Judging Sessions';
	private static $model_importers = array();

	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id = null, $fields = null);
		$modelClass = $this->modelClass;
		if (method_exists($modelClass, 'get_gridfieldconfig')) {
			$form
				->Fields()
				->fieldByName($this->sanitiseClassName($this->modelClass))
				->setConfig($modelClass::get_gridfieldconfig());
		}

		return $form;
	}

//    public function getExportFields() {
//        $modelClass = $this->modelClass;
//        if ($modelClass == 'Beer') {
//            return array(
//                'ID' => 'ID',
//                'Brewer.Name' => 'Brewer Name',
//                'Brewer.Email' => 'Brewer Email',
//                'Name' => 'Beer Name',
//                'ABV' => 'ABV',
//                'BjcpCategory.CategoryNumber' => 'Category',
//                'JudgingSession.SessionDate' => 'Session',
//                'BjcpRating.ID' => 'ResultID'
//            );
//        } else {
//            return parent::getExportFields();
//        }
//    }
}
