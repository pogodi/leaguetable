<?php


use SilverStripe\Dev\BuildTask;

class ImportBJCPTask extends BuildTask
{

    /**
     * Implement this method in the task subclass to
     * execute via the TaskRunner
     *
     * @param \SilverStripe\Control\HTTPRequest $request
     * @return
     */
    public function run($request)
    {
        $loader = BjcpBulkLoader::create('BjcpCategory');
        $result = $loader->load(\SilverStripe\Control\Director::baseFolder() . '/app/src/data/2015_Guidelines.csv');

    }
}
