<?php


use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;
use SilverStripe\Dev\BuildTask;

class ImportResultsTask extends BuildTask {

    /**
     * Implement this method in the task subclass to
     * execute via the TaskRunner
     *
     * @param \SilverStripe\Control\HTTPRequest $request
     *
     * @return
     */
    public function run($request) {
        // pull imap emails, check for attachments in pdf, save to incoming folder
        $mailServer   = Config::inst()->get(ImportResultsTask::class, 'results_server');
        $mailUser     = Config::inst()->get(ImportResultsTask::class, 'results_user');
        $mailPass     = Config::inst()->get(ImportResultsTask::class, 'results_password');
        $targetFolder = Config::inst()->get(ImportResultsTask::class, 'target_folder');
        $targetFolder = ASSETS_PATH . '/' . $targetFolder . '/';

        $options                 = new ezcMailImapTransportOptions();
        $options->ssl            = true;
        $options->uidReferencing = true;

        $imap = new ezcMailImapTransport($mailServer, null, $options);
        $imap->authenticate($mailUser, $mailPass);
        $imap->selectMailbox('Inbox');
        $imap->status($num, $size, $recent, $unseen);
        $ids = $imap->listUniqueIdentifiers();

        foreach ($ids as $id) {
            $set = $imap->fetchByMessageNr($id);

            $parser = new ezcMailParser();
            $inBox  = $parser->parseMail($set);

            $num = 1;
            foreach ($inBox as $mail) {
                $parts       = $mail->fetchParts();
                $subject     = $mail->subject;
                $attachments = [];
                foreach ($parts as $part) {
                    if ($part instanceof ezcMailFile) {
                        rename($part->fileName, $targetFolder . basename($part->contentDisposition->displayFileName));
                        $attachments[] = $targetFolder . basename($part->contentDisposition->displayFileName);
                    }
                }

                $messageID = $imap->listUniqueIdentifiers($num);
                $messageID = reset($messageID);

                if (count($attachments) == 1 && intval($subject) > 0 && strlen($subject) < 8 && strlen($subject) > 0) {
                    $entryNumber = intval($subject);
                    $beer        = Beer::get()->filter('ID', $entryNumber)->first();
                    if ($beer && ! $beer->BjcpRatingID) {

                        if (strpos($attachments[0], '.jpg') !== false) {
                            $file = new Image();
                            $is_image = true;
                        } else {
                            $file = new File();
                            $is_image = false;
                        }

                        $file->setFromLocalFile($attachments[0]);
                        $file->write();

                        $rating         = new BjcpRating();
                        $rating->ScanID = $file->ID;
                        $rating->write();

                        $beer->BjcpRatingID = $rating->ID;
                        $beer->write();
                    }
                }
//                if ( ! count($attachments)) {
//                    $id     = $imap->listUniqueIdentifiers($num);
//                    $id = reset($id);
//                    $result = $imap->delete($id);
//                }
                $result = $imap->delete($messageID);
                $num ++;
            }
        }

        $imap->expunge();
    }
}
